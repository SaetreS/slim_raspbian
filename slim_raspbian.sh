#!/bin/bash

# Edu-related packages
pkgs="
debian-reference-en dillo x2x
scratch nuscratch
timidity
smartsim penguinspuzzle
pistore
sonic-pi
minecraft-pi python-minecraftpi
wolfram-engine
"

for i in $pkgs; do
	echo apt-get -y remove --purge $i
done

dpkg --list |grep "^rc" | cut -d " " -f 3 | xargs dpkg --purge

echo apt-get update -y
echo apt-get upgrade -y
echo apt-get -y autoremove